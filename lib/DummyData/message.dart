class Message {
  final String message;
  final String date;
  final bool selfMessage;

 const Message(
      {required this.message, required this.date, required this.selfMessage});
}
