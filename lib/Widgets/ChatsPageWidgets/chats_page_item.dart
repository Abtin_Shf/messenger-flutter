import 'package:flutter/material.dart';
import 'package:flutter/rendering.dart';
import 'package:messenger/screens/conversation_page.dart';

class ChatsPageItem extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Padding(
      padding: const EdgeInsets.all(4.0),
      child: ListTile(
        onTap: () {
          Navigator.of(context).pushNamed(ConversationPage.routName);
        },
        leading: Container(
          height: 50,
          width: 50,
          child: CircleAvatar(
            backgroundImage: AssetImage('view.png'),
          ),
        ),
        title: Text(
          'Abtin Shafiei',
          style: TextStyle(
            fontSize: 20,
          ),
        ),
        subtitle: Text(
          "hiiiiii!!!! Where Are You???",
          style: TextStyle(
            fontSize: 16,
          ),
        ),
        trailing: Text(
          '12:12',
          style: TextStyle(
            fontSize: 16,
          ),
        ),
      ),
    );
  }
}

Route _rtlSlideTransition(Widget page) {
  return PageRouteBuilder(
    pageBuilder: (context, animation, secondaryAnimation) => page,
    transitionsBuilder: (context, animation, secondaryAnimation, child) {
      const begin = Offset(1.0, 0.0);
      const end = Offset.zero;
      const curve = Curves.ease;
      var tween = Tween(begin: begin, end: end).chain(CurveTween(curve: curve));
      return SlideTransition(
        position: animation.drive(tween),
        child: child,
      );
    },
    transitionDuration: Duration(milliseconds: 700),
  );
}
