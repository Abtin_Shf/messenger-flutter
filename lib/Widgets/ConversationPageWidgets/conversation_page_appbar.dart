import 'package:flutter/material.dart';
import 'package:messenger/screens/chats_page.dart';

class ConversationPageAppBar extends StatelessWidget
    implements PreferredSizeWidget {
  final double height = 60;

  @override
  Widget build(BuildContext context) {
    return AppBar(
      leadingWidth: 30,
      leading: IconButton(
        onPressed: () {
          Navigator.of(context).pushNamed(ChatsPage.routName);
        },
        icon: Icon(
          Icons.arrow_back,
          size: 30,
        ),
      ),
      title: Row(
        children: [
          Container(
            height: height,
            width: height,
            padding: EdgeInsets.all(5),
            child: CircleAvatar(
              backgroundImage: Image.asset("view.png").image,
            ),
          ),
          SizedBox(
            width: 10,
          ),
          Column(
            crossAxisAlignment: CrossAxisAlignment.start,

            children: [
              Text(
                "Abtin Shafiei",
                style: TextStyle(fontSize: 18),
              ),
              Text(
                "@Abtin_Shf",
                style: TextStyle(fontSize: 12),
              ),
            ],
          ),
        ],
      ),
      actions: [
        IconButton(onPressed: () {}, icon: Icon(Icons.more_vert_rounded))
      ],
    );
  }

  @override
  Size get preferredSize => Size.fromHeight(height);
}
