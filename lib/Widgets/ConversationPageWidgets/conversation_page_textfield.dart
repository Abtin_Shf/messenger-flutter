import 'package:flutter/material.dart';

class ConversationPageTextField extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    final width = MediaQuery.of(context).size.width;
    final height = MediaQuery.of(context).size.height;
    return Container(
      decoration: BoxDecoration(
        color: Color.fromRGBO(30 , 45, 84, 0.83),
      ),
      height: height / 16,
      width: width,
      child: Row(
        children: [
          Container(
            width: width / 10,
            height: height / 17,
            child: IconButton(
              onPressed: () {},
              icon: Icon(
                Icons.emoji_emotions_outlined,
                size: 30,
                color: Colors.black54,
              ),
            ),
          ),
          Container(
            width: width * 7 / 10,
            constraints: BoxConstraints(
              maxHeight: 300.0,
            ),
            child: TextField(
              maxLines: null,
              decoration: InputDecoration.collapsed(
                hintText: ("Message"),
              ),
              style: TextStyle(
                fontSize: 18,
                color: Colors.white70,
              ),
            ),
          ),
          Container(
            width: width / 10,
            height: height / 17,
            child: IconButton(
              onPressed: () {},
              icon: Icon(
                Icons.attach_file_outlined,
                size: 30,
                color: Colors.black54,
              ),
            ),
          ),
          Container(
            width: width / 10,
            height: height / 17,
            child: IconButton(
              onPressed: () {},
              icon: Icon(
                Icons.mic_rounded,
                size: 30,
                color: Colors.black54,
              ),
            ),
          ),
        ],
      ),
    );
  }
}
/*
*
    *
    *    return ListTile(
      horizontalTitleGap: 2,
      tileColor: Colors.black12,
      leading:  IconButton(
        onPressed: () {},
        icon: Icon(Icons.emoji_emotions_outlined, size: 30,),
      ),
      title: TextField(
        decoration: InputDecoration(
          hintText: "Message"
        ),
      ),

      trailing: Row(
        children: [
          Expanded(child: IconButton(onPressed: (){}, icon: Icon(Icons.ac_unit,size: 30,),)),
          Expanded(child: IconButton(onPressed: (){}, icon: Icon(Icons.ac_unit,size: 30,),)),
        ],
      ),
    );
    * */
