import 'package:flutter/material.dart';
import 'package:messenger/StaticData/Palette.dart';

class ConversationPageMessageItem extends StatelessWidget {
  final String message;
  final String date;
  final bool selfMessage;

  ConversationPageMessageItem(
      {required this.message, required this.date, required this.selfMessage});

  @override
  Widget build(BuildContext context) {
    final width = MediaQuery.of(context).size.width;
    return Container(
      child: Row(
        mainAxisAlignment:
            selfMessage ? MainAxisAlignment.end : MainAxisAlignment.start,
        children: [
          Column(
            crossAxisAlignment:
                selfMessage ? CrossAxisAlignment.start : CrossAxisAlignment.end,
            children: [
              Container(
                width: width /2 -20,
                padding: EdgeInsets.fromLTRB(15.0, 10.0, 15.0, 10.0),
                decoration: BoxDecoration(
                  color: selfMessage
                      ? Palette.selfMessageColor
                      : Palette.receivedMessageColor,
                  borderRadius: BorderRadius.circular(8.0),

                ),
                margin: EdgeInsets.only(left: 10),
                child: Text(message, style: TextStyle(
                  color: Palette.textColor,
                  fontSize: 16,

                ),),
              ),
              Container(
                child: Text(date, style: TextStyle(
                  color: Colors.white,


                ),),
                margin: EdgeInsets.only(left: 10, right: 10,bottom: 10),
              ),
            ],
          ),
        ],
      ),
    );
  }
}
