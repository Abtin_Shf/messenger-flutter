import 'package:flutter/material.dart';

class Palette {
  static const textColor = Colors.white;
  static const selfMessageColor = Color.fromRGBO(19, 161, 208, 1.0);
  static const receivedMessageColor = Color.fromRGBO(8, 50, 206, 1.0);
}
