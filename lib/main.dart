import 'package:firebase_core/firebase_core.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:messenger/screens/chats_page.dart';
import 'package:messenger/screens/conversation_page.dart';
import 'package:messenger/screens/intro_page.dart';
import 'package:messenger/screens/login_page.dart';
import 'package:page_transition/page_transition.dart';

Future<void> main() async {
  WidgetsFlutterBinding.ensureInitialized();
  await Firebase.initializeApp();
  SystemChrome.setPreferredOrientations([
    DeviceOrientation.portraitUp,
    DeviceOrientation.portraitDown,
  ]);
  runApp(Messenger());
}

class Messenger extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      debugShowCheckedModeBanner: false,
      title: 'Flutter Demo',
      home: IntroPage(),
      routes: {
        LoginPage.routName: (ctx) => LoginPage(),
      },
      onGenerateRoute: (settings) {
        switch (settings.name) {
          case ConversationPage.routName:
            return PageTransition(
              child: ConversationPage(),
              type: PageTransitionType.rightToLeftWithFade,
              //duration: Duration(milliseconds: 200),
              reverseDuration: Duration(milliseconds: 200),
            );
          case ChatsPage.routName:
            return PageTransition(
              child: ChatsPage(),
              type: PageTransitionType.leftToRightJoined,
              childCurrent: ConversationPage(),
              reverseDuration: Duration(milliseconds: 400),
            );

          default:
            return null;
        }
      },
    );
  }
}
