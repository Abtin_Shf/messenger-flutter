import 'package:flutter/material.dart';
import 'package:messenger/Widgets/ChatsPageWidgets/chats_page_drawer.dart';
import 'package:messenger/Widgets/ChatsPageWidgets/chats_page_item.dart';

class ChatsPage extends StatelessWidget {
  static const String routName = '/chats-page';

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text("AbtGram"),
        actions: [IconButton(onPressed: () {}, icon: Icon(Icons.search))],
      ),
      body: ListView.separated(
        shrinkWrap: true,
        physics: ClampingScrollPhysics(),
        itemBuilder: (ctx, index) {
          return ChatsPageItem();
        },
        itemCount: 15,
        separatorBuilder: (context, index) => Padding(
          padding: EdgeInsets.only(left: 75, right: 20),
          child: Divider(
            color: Colors.black45,
          ),
        ),
      ),
      drawerEdgeDragWidth: MediaQuery.of(context).size.width,
      drawer: ChatsPageDrawer(),
    );
  }
}
