import 'package:firebase_auth/firebase_auth.dart';
import 'package:flutter/material.dart';
import 'package:flutter/painting.dart';
import 'package:flutter_svg/svg.dart';
import 'package:messenger/screens/intro_page.dart';

import 'package:simple_shadow/simple_shadow.dart';

class SignUpPage extends StatefulWidget {
  static const String routName = "/signup-page";

  @override
  _SignUpPageState createState() => _SignUpPageState();
}

class _SignUpPageState extends State<SignUpPage> {
  bool _obscureText = true;

  void _toggle() {
    setState(() {
      _obscureText = !_obscureText;
    });
  }

  final GlobalKey<FormState> _loginFormKey = GlobalKey();
  final _usernameController = TextEditingController();
  final _passwordController = TextEditingController();
  final _confirmPasswordController = TextEditingController();

  @override
  Widget build(BuildContext context) {
    final _screenHeight = MediaQuery.of(context).size.height;
    final _screenWidth = MediaQuery.of(context).size.width;

    final icon = SimpleShadow(
      child: SvgPicture.asset(
        "assets/chat.svg",
        color: Color.fromRGBO(4, 62, 149, 1.0),
        height: _screenHeight * 0.16,
        width: _screenWidth * 0.15,
      ),
      opacity: 0.7,
      offset: Offset(7, 10),
    );

    return GestureDetector(
      onTap: () => FocusScope.of(context).requestFocus(new FocusNode()),
      child: Scaffold(
        resizeToAvoidBottomInset: false,
        body: Container(
          decoration: BoxDecoration(
            image: DecorationImage(
              image: AssetImage("view.png"),
              fit: BoxFit.cover,
            ),
          ),
          child: Column(
            children: <Widget>[
              SizedBox(height: _screenHeight * 0.12),
              GestureDetector(
                child: icon,
                onTap: () {
                  Navigator.of(context)
                      .pushReplacement(_pageRoute(IntroPage()));
                },
              ),
              SizedBox(height: _screenHeight * 0.025),
              SimpleShadow(
                opacity: 0.7,
                offset: Offset(7, 10),
                child: Material(
                  color: Colors.transparent,
                  child: Text(
                    "SignUp",
                    textAlign: TextAlign.center,
                    style: TextStyle(
                      fontSize: 26,
                      color: Colors.white,
                      fontWeight: FontWeight.w300,
                    ),
                  ),
                ),
              ),
              SizedBox(height: _screenHeight * 0.025),
              Form(
                key: _loginFormKey,
                child: Column(
                  crossAxisAlignment: CrossAxisAlignment.center,
                  children: <Widget>[
                    Container(
                      width: 340,
                      child: SimpleShadow(
                        opacity: 0.7,
                        offset: Offset(7, 10),
//=====================================================================
                        child: TextFormField(
                          controller: _usernameController,
                          onChanged: (value) {
                            _loginFormKey.currentState!.validate();
                          },
                          validator: (String? value) {
                            if (value != null && value.trim().contains(" ")) {
                              return "Email Can't Contain Spaces!";
                            }
                            return null;
                          },
                          decoration: InputDecoration(
                            errorStyle: TextStyle(
                                color: Colors.red.shade900, fontSize: 14),
                            hintText: "Email...",
                            hintStyle: TextStyle(
                                color: Colors.white,
                                fontWeight: FontWeight.w200,
                                fontSize: 25),
                            fillColor: Color.fromRGBO(30, 45, 84, 0.83),
                            filled: true,
                            border: null,
                          ),
                          style: TextStyle(
                            color: Colors.white,
                            fontSize: 24,
                            fontWeight: FontWeight.w300,
                          ),
                        ),
                      ),
                    ),
                    SizedBox(height: _screenHeight * 0.025),
//===================================================================================
                    Container(
                      width: 340,
                      //height: 60,
                      child: SimpleShadow(
                        opacity: 0.7,
                        offset: Offset(7, 10),
                        child: TextFormField(
                          obscureText: _obscureText,
                          controller: _passwordController,
                          validator: (value) {
                            if (value!.isNotEmpty && value.length < 6) {
                              return "Password Must be 6 or More Characters!";
                            }
                            return null;
                          },
                          decoration: InputDecoration(
                            suffixIcon: GestureDetector(
                              child: Icon(
                                _obscureText
                                    ? Icons.visibility_off
                                    : Icons.visibility,
                                color: Color.fromRGBO(16, 152, 251, 0.70),
                              ),
                              onTap: () {
                                _toggle();
                              },
                            ),
                            errorStyle: TextStyle(
                                color: Colors.red.shade900, fontSize: 14),
                            hintText: "Password...",
                            hintStyle: TextStyle(
                                color: Colors.white,
                                fontWeight: FontWeight.w200,
                                fontSize: 25),
                            fillColor: Color.fromRGBO(30, 45, 84, 0.83),
                            filled: true,
                            border: null,
                          ),
                          style: TextStyle(
                              color: Colors.white,
                              fontSize: 24,
                              fontWeight: FontWeight.w300),
                        ),
                      ),
                    ),
                    SizedBox(height: _screenHeight * 0.025),
                    Container(
                      width: 340,
                      //height: 60,
                      child: SimpleShadow(
                        opacity: 0.7,
                        offset: Offset(7, 10),
                        child: TextFormField(
                          obscureText: _obscureText,
                          controller: _confirmPasswordController,
                          validator: (value) {
                            if (value!.isNotEmpty && value.length < 6) {
                              return "Password Must be 6 or More Characters!";
                            }
                            return null;
                          },
                          decoration: InputDecoration(
                            suffixIcon: GestureDetector(
                              child: Icon(
                                _obscureText
                                    ? Icons.visibility_off
                                    : Icons.visibility,
                                color: Color.fromRGBO(16, 152, 251, 0.70),
                              ),
                              onTap: () {
                                _toggle();
                              },
                            ),
                            errorStyle: TextStyle(
                                color: Colors.red.shade900, fontSize: 14),
                            hintText: "Confirm Password...",
                            hintStyle: TextStyle(
                                color: Colors.white,
                                fontWeight: FontWeight.w200,
                                fontSize: 25),
                            fillColor: Color.fromRGBO(30, 45, 84, 0.83),
                            filled: true,
                            border: null,
                          ),
                          style: TextStyle(
                              color: Colors.white,
                              fontSize: 24,
                              fontWeight: FontWeight.w300),
                        ),
                      ),
                    ),
                    SizedBox(height: _screenHeight * 0.025),
                    Container(
                      width: _screenWidth,
                    ),
                    Container(
                      width: 250,
                      height: 60,
                      child: ElevatedButton(
                        style: ButtonStyle(
                          shape: MaterialStateProperty.all<
                                  RoundedRectangleBorder>(
                              RoundedRectangleBorder(
                                  borderRadius: BorderRadius.circular(8.0),
                                  side: BorderSide(color: Colors.transparent))),
                          backgroundColor: MaterialStateProperty.all<Color>(
                            Color.fromRGBO(22, 62, 139, 0.8),
                          ),
                          foregroundColor:
                              MaterialStateProperty.all<Color>(Colors.white),
                        ),
                        onPressed: () {
                          bool v = _loginFormKey.currentState!.validate();
                          print(v);
                          if (v) {
                            _submit();
                          }
                        },
                        child: Text(
                          "Login",
                          style: TextStyle(
                            fontWeight: FontWeight.w200,
                            color: Colors.white,
                            fontSize: 24,
                          ),
                        ),
                      ),
                    ),
                  ],
                ),
              ),
            ],
          ),
        ),
      ),
    );
  }

  @override
  void dispose() {
    _usernameController.dispose();
    _passwordController.dispose();
    _confirmPasswordController.dispose();
    super.dispose();
  }

  Future<void> _submit() async{
    try {
      UserCredential userCredential = await FirebaseAuth.instance.createUserWithEmailAndPassword(
          email: _usernameController.text,
          password: _passwordController.text,
      );
    } on FirebaseAuthException catch (e) {
      if (e.code == 'weak-password') {
        print('The password provided is too weak.');
      } else if (e.code == 'email-already-in-use') {
        print('The account already exists for that email.');
      }
    } catch (e) {
      print(e);
    }

    print(_usernameController.text);
    print(_passwordController.text);
  }
}

Route _pageRoute(page) {
  return PageRouteBuilder(
    pageBuilder: (c, a1, a2) => page,
    transitionsBuilder: (c, anim, a2, child) =>
        FadeTransition(opacity: anim, child: child),
    transitionDuration: Duration(milliseconds: 1000),
  );
}
