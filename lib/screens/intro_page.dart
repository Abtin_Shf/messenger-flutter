import 'package:flutter/material.dart';
import 'package:flutter_svg/svg.dart';
import 'package:messenger/screens/chats_page.dart';
import 'package:messenger/screens/login_page.dart';
import 'package:messenger/screens/signup_page.dart';
import 'package:simple_shadow/simple_shadow.dart';
import 'package:outline_gradient_button/outline_gradient_button.dart';

class IntroPage extends StatefulWidget {
  @override
  _IntroPageState createState() => _IntroPageState();
}

class _IntroPageState extends State<IntroPage> {
  @override
  Widget build(BuildContext context) {
    final _screenHeight = MediaQuery.of(context).size.height;
    final _screenWidth = MediaQuery.of(context).size.width;
    final icon = SimpleShadow(
      child: SvgPicture.asset(
        "assets/chat.svg",
        color: Color.fromRGBO(4, 62, 149, 1.0),
        height: _screenHeight * 0.16,
        width: _screenWidth * 0.15,
      ),
      opacity: 0.7,
      offset: Offset(7, 10),
    );

    final btnLogin = OutlineGradientButton(
      onTap: () {
        Navigator.of(context).push(_pageRoute(LoginPage()));
      },
      gradient: LinearGradient(
        colors: [Colors.orange, Colors.yellow],
        begin: Alignment(-1, -1),
        end: Alignment(2, 2),
      ),
      strokeWidth: 2,
      padding: EdgeInsets.symmetric(horizontal: 24, vertical: 12),
      radius: Radius.circular(4),
      child: Container(
        width: _screenWidth * 0.63,
        height: _screenHeight * 0.045,
        child: Center(
          child: Text(
            "Login",
            textAlign: TextAlign.center,
            style: TextStyle(
                fontSize: 26, color: Colors.white, fontWeight: FontWeight.w300),
          ),
        ),
      ),
    );
    final btnSignUp = OutlineGradientButton(
      onTap: () {
        Navigator.of(context).push(_pageRoute(SignUpPage()));
      },
      gradient: LinearGradient(
        colors: [Colors.orange, Colors.yellow],
        begin: Alignment(-1, -1),
        end: Alignment(2, 2),
      ),
      strokeWidth: 2,
      padding: EdgeInsets.symmetric(horizontal: 24, vertical: 12),
      radius: Radius.circular(4),
      child: Container(
        width: _screenWidth * 0.63,
        height: _screenHeight * 0.045,
        child: Center(
          child: Text(
            "SignUp",
            textAlign: TextAlign.center,
            style: TextStyle(
                fontSize: 26, color: Colors.white, fontWeight: FontWeight.w300),
          ),
        ),
      ),
    );
    return Container(
      decoration: BoxDecoration(
        image: DecorationImage(
          image: AssetImage("view.png"),
          fit: BoxFit.cover,
        ),
      ),
      child: Column(
        children: <Widget>[
          SizedBox(height: _screenHeight * 0.12),
          GestureDetector(
            child: icon,
            onTap: (){
              Navigator.of(context).pushReplacement(_slidingPageRout(ChatsPage()));
            },
          ),
          SizedBox(height: _screenHeight * 0.025),
          SimpleShadow(
            opacity: 0.7,
            offset: Offset(7, 10),
            child: Material(
              color: Colors.transparent,
              child: Text(
                "AbtGram",
                textAlign: TextAlign.center,
                style: TextStyle(
                    fontSize: 26,
                    color: Colors.white,
                    fontWeight: FontWeight.w300),
              ),
            ),
          ),
          SizedBox(height: _screenHeight * 0.06),
          SimpleShadow(opacity: 0.7, offset: Offset(7, 10), child: btnLogin),
          SizedBox(height: _screenHeight * 0.06),
          SimpleShadow(
            child: btnSignUp,
            opacity: 0.7,
            offset: Offset(7, 10),
          ),
        ],
      ),
    );
  }
}

Route _pageRoute(page) {
  return PageRouteBuilder(
    pageBuilder: (c, a1, a2) => page,
    transitionsBuilder: (c, anim, a2, child) =>
        FadeTransition(opacity: anim, child: child),
    transitionDuration: Duration(milliseconds: 1000),
  );
}

Route _slidingPageRout(page) {
  return PageRouteBuilder(
    pageBuilder: (context, animation, secondaryAnimation) => page,
    transitionsBuilder: (context, animation, secondaryAnimation, child) {
      const begin = Offset(0.0, 1.0);
      const end = Offset.zero;
      const curve = Curves.ease;
      final tween = Tween(begin: begin, end: end);
      final curvedAnimation = CurvedAnimation(
        parent: animation,
        curve: curve,
      );
      return SlideTransition(
        position: tween.animate(curvedAnimation),
        child: child,
      );
    },
  );
}
