import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:messenger/DummyData/messages.dart';
import 'package:messenger/Widgets/ChatsPageWidgets/chats_page_item.dart';
import 'package:messenger/Widgets/ConversationPageWidgets/conversation_page_appbar.dart';
import 'package:messenger/Widgets/ConversationPageWidgets/conversation_page_message_item.dart';
import 'package:messenger/Widgets/ConversationPageWidgets/conversation_page_textfield.dart';

import 'package:messenger/screens/chats_page.dart';

class ConversationPage extends StatefulWidget {
  static const routName = '/conversation-page';

  @override
  _ConversationPageState createState() => _ConversationPageState();
}

class _ConversationPageState extends State<ConversationPage> {
  @override
  Widget build(BuildContext context) {
    final height = MediaQuery.of(context).size.height;
    return Dismissible(
      direction: DismissDirection.startToEnd,
      key: UniqueKey(),
      background: ChatsPage(),
      child: Container(
        decoration: BoxDecoration(
          image: DecorationImage(
            image: AssetImage("galaxy.png"),
            fit: BoxFit.fill,
          ),
        ),
        child: Scaffold(
          resizeToAvoidBottomInset: false,
          appBar: ConversationPageAppBar(),
          body: Container(
            decoration: BoxDecoration(
              image: DecorationImage(
                image: AssetImage("galaxy.png"),
                fit: BoxFit.fill,
              ),
            ),
            child: Column(
              children: [
                Container(
                  height: height -
                      (height / 16) -
                      60 -
                      MediaQuery.of(context).padding.top,
                  child: ListView.builder(
                      reverse: true,
                      itemCount: DUMMY_MESSAGES.length,
                      itemBuilder: (context, i) {
                        final dm =DUMMY_MESSAGES[i];
                        return ConversationPageMessageItem(message: dm.message, date: dm.date, selfMessage: dm.selfMessage);
                      }),
                ),
                Container(
                  height: height / 16,
                  child: ConversationPageTextField(),
                ),
              ],
            ),
          ),
        ),
      ),
    );
  }
}
